# Bus signal priority embeded code
This C++ code is embeded in Run-time-extension (RTE) in [CORSIM](https://mctrans.ce.ufl.edu/mct/index.php/tsis-corsim/), a microscopic traffic simulator. The model considers green time extension, red time truncation, and compensation. 

## File description
cardtrf: define which signals are bus-priority activated. 
network: read the simulated network, trace the bus position, and assign the signal action (extend green or truncate red time)to PrioritySignal.
node: store and trace the signal time
phase: store and trace the signal phase time
PrioritySignal: signal decision making.
vehicle: store and trace the car and truck position. 
Network folder: a [CORSIM](https://mctrans.ce.ufl.edu/mct/index.php/tsis-corsim/) simulated network.

