#ifndef CVehicle_H
#define CVehicle_H
/*******************************************************************************

   Copyright (c) 1997-2003
   ITT Industries, Systems Division, All rights reserved.

   This software is disseminated under the sponsorship of the Department of
   Transportation in the interest of information exchange.  The United States
   Government and ITT Industries assume no liability for its contents or use.

   Application: RTTRACS Run-Time Extension

   $Workfile: node.h $

   Description: Header file for the CNode class.
   
   $History: node.h $
  
 *****************  Version 6  ***************** 
 User: Tomich       Date: 9/25/03    Time: 4:59p 
 Updated in $/TSIS Development/Run-Time Extensions/fixed time 
 Cleaned up code structure. 
  
 *****************  Version 5  ***************** 
 User: Tomich       Date: 11/22/02   Time: 4:56p 
 Updated in $/TSIS Development/Run-Time Extensions/fixed time 
 Cleaned up based on Lint suggestions. 
  
 *****************  Version 4  ***************** 
 User: Tomich       Date: 11/21/02   Time: 5:42p 
 Updated in $/TSIS Development/Run-Time Extensions/fixed time 
 Added #ifndef bracketing for the header file. 
  
 *****************  Version 3  ***************** 
 User: Tomich       Date: 11/21/02   Time: 5:27p 
 Updated in $/TSIS Development/Run-Time Extensions/fixed time 
 Replace tabs with spaces.  Added file prologues. 

 ******************************************************************************/

// Define constants for the maximum number of approaches
// and fixed-time signal intervals.
#include <deque>
#include <iostream>
using namespace std;
class CVehicle
{
   public:
      CVehicle();
      virtual ~CVehicle();
	  int ID;
	  void VehicleID(int);
	  int returnID();
	  void setstartTime(int);
	  void setendTime(int);
	  void BusLocation(int, int, int);
	  int startTime();
	  int endTime();
	  deque <CVehicle*> buslocation;
	  deque <CVehicle*> autolocation;
	  int orgtime;
	  int desttime;
	  int returnTravelTime();

	  void VehiclePosition(int, int, int, int, int, int);
	  int returnStartUp();
	  int returnStartDn();
	  int returnStartTime();
	  int returnEndUp();
	  int returnEndDn();
	  int returnEndTime();

	  int StartUp;
	  int StartDn;
	  int StartTime;
	  int EndUp;
	  int EndDn;
	  int EndTime;

};

#endif // CNode_H
