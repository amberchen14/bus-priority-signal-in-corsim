
#include "vehicle.h"


CVehicle::CVehicle() {}
CVehicle::~CVehicle()
{
}


void CVehicle::VehicleID(int id)
{
	orgtime = 0;
	desttime = 0;
	ID=id;
}
int CVehicle::returnID()
{
	return ID;
}


void CVehicle::setstartTime(int time)
{
	if(orgtime ==0)
	{
		orgtime=time;
	}
}

void CVehicle::setendTime(int time)
{
	if(desttime ==0)
	{
		desttime=time;
	}

}

int CVehicle::startTime()
{
	return orgtime;
}


int CVehicle::endTime()
{
	return desttime;
}

int CVehicle::returnTravelTime()
{
	int traveltime =0;
	if (desttime != 0)
	{
		traveltime=desttime-orgtime;
		return traveltime;

	}
	return -1;

}

void CVehicle::VehiclePosition(int starttime, int start_upnode, int start_dnnode,  int endtime, int end_upnode, int end_dnnode)
{
	  StartTime = starttime;
	  StartUp = start_upnode;
	  StartDn = start_dnnode;
	  EndTime = endtime;
	  EndUp = end_upnode;
	  EndDn = end_dnnode;
}
int CVehicle::returnStartUp()
{
	return StartUp;
}
int CVehicle::returnStartDn()
{
	return StartDn;
}
int CVehicle::returnStartTime()
{
	return StartTime;
}
int CVehicle::returnEndUp()
{
	return EndUp;

}
int CVehicle::returnEndDn()
{
	return EndDn;

}
int CVehicle::returnEndTime()
{
	return EndTime;
}