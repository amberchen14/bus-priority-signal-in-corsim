#ifndef CNode_H
#define CNode_H
/*******************************************************************************

   Copyright (c) 1997-2003
   ITT Industries, Systems Division, All rights reserved.

   This software is disseminated under the sponsorship of the Department of
   Transportation in the interest of information exchange.  The United States
   Government and ITT Industries assume no liability for its contents or use.

   Application: RTTRACS Run-Time Extension

   $Workfile: node.h $

   Description: Header file for the CNode class.
   
   $History: node.h $
  
 *****************  Version 6  ***************** 
 User: Tomich       Date: 9/25/03    Time: 4:59p 
 Updated in $/TSIS Development/Run-Time Extensions/fixed time 
 Cleaned up code structure. 
  
 *****************  Version 5  ***************** 
 User: Tomich       Date: 11/22/02   Time: 4:56p 
 Updated in $/TSIS Development/Run-Time Extensions/fixed time 
 Cleaned up based on Lint suggestions. 
  
 *****************  Version 4  ***************** 
 User: Tomich       Date: 11/21/02   Time: 5:42p 
 Updated in $/TSIS Development/Run-Time Extensions/fixed time 
 Added #ifndef bracketing for the header file. 
  
 *****************  Version 3  ***************** 
 User: Tomich       Date: 11/21/02   Time: 5:27p 
 Updated in $/TSIS Development/Run-Time Extensions/fixed time 
 Replace tabs with spaces.  Added file prologues. 

 ******************************************************************************/

// Define constants for the maximum number of approaches
// and fixed-time signal intervals.


const int maxApproaches = 5;
const int maxIntervals  = 12;
const int minGreenTime = 9;


class CPhase;

class CNode : public CObject
{
   public:

	  //bus prioirty system
	void CNode::setPhaseTime();
	int actualphasetime[12] ; //default
	int expectphasetime[12]; // realtime
	int fixedphasetime[12];
	int actualcycle;
	int expectcycle;
	int difphasetime[12];
	int getPhaseTime(int phase);
	int UpdateSignal(int ntime);
	int getCycleTime();
	int currentphase;
	int getPhaseTime();
	int getPhase();
	void resetCycle(int ntime);
	int GrnExtension(int);
	int RedTruncation(int bus_distance);

	int timespend;
	int keep;
	int timegap;
	void reportCycle();
	int recordCycle(int);

	std::deque <CPhase> cyclelist;
	std::deque <CPhase> ::iterator  cycle_pos;

#endif // CNode_H
