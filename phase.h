#ifndef CPhase_H
#define CPhase_H

#include <iostream>
#include <string>
#include <fstream>
using namespace std;
#pragma once
#include <vector>


class CPhase
{
	public: 
	 CPhase(void);
	~ CPhase(void);

	int PhaseTime1;
	int PhaseTime2;
	int Cycle;
	int Time;

	void record(int, int, int, int);
	int getCycle(){return Cycle;};
	int getPhaseTime1(){return PhaseTime1;};
	int getPhaseTime2(){return PhaseTime2;};
	int getTime(){return Time;};


};
#endif