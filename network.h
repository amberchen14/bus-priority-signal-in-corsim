#ifndef CNetwork_H
#define CNetwork_H
/*******************************************************************************

   Copyright (c) 1997-2003
   ITT Industries, Systems Division, All rights reserved.

   This software is disseminated under the sponsorship of the Department of
   Transportation in the interest of information exchange.  The United States
   Government and ITT Industries assume no liability for its contents or use.

   Application: RTTRACS Run-Time Extension

   $Workfile: network.h $

   Description: Header file for the CNetwork class.
   
   $History: network.h $
  
 *****************  Version 5  ***************** 
 User: Tomich       Date: 9/26/03    Time: 10:42a 
 Updated in $/TSIS Development/Run-Time Extensions/fixed time 
 Cleaned up code structure. 
  
 *****************  Version 4  ***************** 
 User: Tomich       Date: 11/25/02   Time: 12:45p 
 Updated in $/TSIS Development/Run-Time Extensions/fixed time 
 Moved MFC include files from this header file to the implementation 
 file.  Added an initializing constructor.  Moved some data to private. 
  
 *****************  Version 3  ***************** 
 User: Tomich       Date: 11/21/02   Time: 5:42p 
 Updated in $/TSIS Development/Run-Time Extensions/fixed time 
 Added #ifndef bracketing for the header file. 
  
 *****************  Version 2  ***************** 
 User: Tomich       Date: 11/21/02   Time: 5:29p 
 Updated in $/TSIS Development/Run-Time Extensions/fixed time 
 Replaced tabs with spaces.  Added file prologues.  Added the 
 ReadTRFLine method.  Cleaned up based on Lint suggestions. 

 ******************************************************************************/
#include "vehicle.h"
#include "cardtrf.h"
#include "PrioritySignal.h"

class cardtrf;
class CPriority;
class CVehicle;

class CNetwork : public CObject
{

	  void   TraceBUSLocation( void );
	  void UpdateNodePrioritySignalStates();
	  void CreateTRF();
	  void RecordType();
	  int RecordType35(int);
	  int RecordType36(int);
	  int TraceBusLocation();
	  int Trigger(int dnode, int flag);
	  void UpdateBusPriorityNodeSignalStates();
	  void ResetSignalTime(int);
	  void SignalCode( int );
	  int  getLeftSignal(){return getLeft;}
	  int  getRightSignal(){return getRight;}
	  int  getThroughSignal(){return getThrough;}
	  int  getDiagSignal(){return getLeftDiag;}
	  int TravelTime();
      void SetRequireData();
	  void ReportTravelTime();

	  int getLeft;
	  int getThrough;
	  int getRight;
	  int getLeftDiag;

	  int originlink;
	  int destlink;
	  int AddGreen(int);
	  int DedGreen(int);
	
	 std:: deque <CVehicle> automobile;
	 std::deque <CVehicle> bus;
	 std:: deque <CVehicle> ::iterator auto_pos;
	 std:: deque <CVehicle> ::iterator bus_pos;
	

	  void UpdatePriorityNodeSignalStates();
	  void InitialSet();
	  
	  void ReportPhase();
	  	  void ReportVolume();

	private:

	  //BPS
	  std::vector<char> pTRF;
	  std::deque <cardtrf> cardlist;	  
	  std:: deque <cardtrf> :: iterator card_pos;
	  std:: deque <cardtrf> :: iterator card_pos2;	

	  std::vector <CPriority> bpslist;
	  std::vector <CPriority> ::iterator bps_pos;

	  std::vector <CNode> bps;
	  std::vector <CNode> ::iterator bps_ps;


	  std::vector <int> busroute[12];
	  std::vector <int> ::iterator bs_pos;
	  std::vector <int> ::iterator bs_pos1;

	  std::vector <int> cyclelist;
	  std::vector <int> sgnlist;
	  std::vector <int> tmlist;
	  std::vector <int> clist[13];


	  std::vector<vector<int>> crosslist;

	  int Type35(int line);
	  int Type36(int line);
	  int Type187(int line);


	  int bus_num[20][7];
	  int routenum;

};

#endif // CNetwork_H
