#ifndef CPriority_H
#define CPriority_H
/*******************************************************************************

   Copyright (c) 1997-2003
   ITT Industries, Systems Division, All rights reserved.

   This software is disseminated under the sponsorship of the Department of
   Transportation in the interest of information exchange.  The United States
   Government and ITT Industries assume no liability for its contents or use.

   Application: RTTRACS Run-Time Extension

   $Workfile: node.h $

   Description: Header file for the CNode class.
   
   $History: node.h $
  
 *****************  Version 6  ***************** 
 User: Tomich       Date: 9/25/03    Time: 4:59p 
 Updated in $/TSIS Development/Run-Time Extensions/fixed time 
 Cleaned up code structure. 
  
 *****************  Version 5  ***************** 
 User: Tomich       Date: 11/22/02   Time: 4:56p 
 Updated in $/TSIS Development/Run-Time Extensions/fixed time 
 Cleaned up based on Lint suggestions. 
  
 *****************  Version 4  ***************** 
 User: Tomich       Date: 11/21/02   Time: 5:42p 
 Updated in $/TSIS Development/Run-Time Extensions/fixed time 
 Added #ifndef bracketing for the header file. 
  
 *****************  Version 3  ***************** 
 User: Tomich       Date: 11/21/02   Time: 5:27p 
 Updated in $/TSIS Development/Run-Time Extensions/fixed time 
 Replace tabs with spaces.  Added file prologues. 

 ******************************************************************************/

// Define constants for the maximum number of approaches
// and fixed-time signal intervals.
#include <deque>
#include <vector>
#include <iostream>
using namespace std;
class CPriority
{
   public:
	   CPriority();
	   virtual ~CPriority();
	   int NodeID;
	   void ID(int);
	   int returnID();

	   void phase_signal(int phase, int order, int signal);
	   int returnPhaseNum();
	   int returnSignal(int phase, int order);
	   void PriorityPhase(int phase);
	   void UnPriorityPhase(int phase);
	   int returnPriority();
	   int returnUnPriority();
	   int returnPhaseTime();
	   void setPhaseTime(int order, int time);
	   void UpdatePhaseTime(int i);
	   void UpdatePhase(int i);
	   int getPhase();
	   void ResetPhaseTime(int i);
	   int AddGrn();
	   int DltRed();
	   int getPhaseTime(int i);
	   int getCycleTime();	   
	   void setCurrentTime(int time);
	   int getTime();
	   int gerUnPrStep();   
	   int realCycle(int i);
	   int getRealCycle();
	   int getRealPhase(int i);
	   
	   int recordTimeGap(int i);
	   int returnTimeGap();	   
	   void downnode(int node);
	   int returndownnode(int i);
	   int recordCrossVehicles(int i);
	   
	   int timegap;

	   int addgrn;
	   int dltred;
	   int mingrn;
	   std::vector <int> dwnnodelist;
	   std::vector <int> ::iterator pos;
	   std::vector <int> ::iterator pos1;
	   std::vector <int> ::iterator pos2;


	   std::vector <int> vehlist;

	   int phasesignal[12][5];
	   int priorityphase;
	   int unpriorityphase;
	   int phasetime[12];
	   int phasetime2[12];
	   int interval;
	   int cycle1;
	   int cycle2;
	   int phase_number;
	   int CurrentTime;
	
	   int realphase[12];
	   int realcycle;
	   int RealPhase;

};

#endif // CNode_H
