void CNode::setPhaseTime()
{
	int node = GetID();
	actualcycle = 0;
    expectcycle = 0;
	timegap = 0;
	for( int iInterval=0; iInterval<maxIntervals; iInterval++ )
	{
		expectphasetime[iInterval] = 0;
		actualphasetime[iInterval] = 0;
		fixedphasetime[iInterval] = 0;
	}
	actualphasetime[1] = 4;
	actualphasetime[2] = 2;
	actualphasetime[4] = 4;
	actualphasetime[5] = 2;

	if (node == 28)
	{
		actualphasetime[0] = 38;	
		actualphasetime[3] = 30;
	}
	if (node == 9)
	{
		actualphasetime[0] = 35;	
		actualphasetime[3] = 33;
	}
	if (node == 8)
	{
		actualphasetime[0] = 33;	
		actualphasetime[3] = 35;
	}
	if (node == 7)
	{
		actualphasetime[0] = 38;	
		actualphasetime[3] = 30;
	}
	if (node == 6)
	{
		actualphasetime[0] = 43;	
		actualphasetime[3] = 25;
	}
	if (node == 5)
	{
		actualphasetime[0] = 35;	
		actualphasetime[3] = 33;
	}
	if (node == 4)
	{
		actualphasetime[0] = 42;	
		actualphasetime[3] = 26;
	}
	if (node == 3)
	{
		actualphasetime[0] = 42;	
		actualphasetime[3] = 26;
	}
	if (node == 2)
	{
		actualphasetime[0] = 43;	
		actualphasetime[3] = 25;
	}

	for( int iInterval=0; iInterval<maxIntervals; iInterval++ )
	{
		expectphasetime[iInterval]  = actualphasetime[iInterval] ;
		fixedphasetime[iInterval]  = actualphasetime[iInterval] ;
		expectcycle += actualphasetime[iInterval];
		difphasetime[iInterval] = actualphasetime[iInterval] -expectphasetime[iInterval] ;
   }
	currentphase = 0;
	timespend = 0;
}

int CNode::getPhaseTime(int phase)
{
	return actualphasetime[phase];
}

int CNode::getCycleTime()
{
	return actualcycle;
}

int CNode::getPhaseTime()
{
	return timespend;
}

int CNode::getPhase()
{
	return currentphase;
}

int CNode::UpdateSignal(int ntime)
{
	int node = GetID();
	if (timespend == actualphasetime[currentphase])
	{
		if (currentphase == 0) return 1; //for get bus distance	
		else if (actualphasetime[currentphase + 1] ==0) 
		{
			recordCycle(ntime);
			resetCycle(ntime);
			currentphase = 0;
		}
		else currentphase++;

		timespend = 1;
		actualcycle ++;;
		return 2;
	}
	else
	{
			if (currentphase == 3  &&  timespend >= actualphasetime[currentphase] - minGreenTime && actualphasetime[currentphase] == fixedphasetime[currentphase] ) 
			{
				timespend++;
				actualcycle++;
				return 3; //for get bus distance	
			}
		//}
		timespend++;
		actualcycle++;
		return 0;
	}
	return 0;
}

void CNode::resetCycle(int ntime)
{
	actualcycle = 0;
	// for no red_truncation's compensation
	int node = GetID();
	int flag1= 0;
	
//	if (node == 4 || node == 5)
//	{
	
		for( int iInterval=0; iInterval<maxIntervals; iInterval++ )
		{
			difphasetime[iInterval]=actualphasetime[iInterval] -expectphasetime[iInterval];
			actualphasetime[iInterval] = fixedphasetime[iInterval] - difphasetime[iInterval];
			expectphasetime[iInterval] = actualphasetime[iInterval];
		}

}

int CNode::GrnExtension(int sec)
{
	/*timespend = 1;
	currentphase++;
	return 2; */
	if (sec == -1 || sec == 0 )
	{
		timespend = 1;
		currentphase++;
		return 2;
	}
	else
	{
		keep = sec;
		actualphasetime[currentphase] += sec;
		actualphasetime[3]-= sec;
		timespend++;
	}
	return 0;
}

int CNode::RedTruncation(int timegap)
{
	int node = GetID();
/*	if (node == 4 || node ==5)
	{*/
		if (timegap >=1)
		{
			actualphasetime[currentphase] = timespend;
			return 2;
		}
	/*}
	else 
	{
		if (timegap >=5)
		{
			actualphasetime[currentphase] = timespend;
			return 2;
		}*/	
	
//	}
	return 0;
}

int CNode::recordCycle(int ntime)
{
	CPhase cphase;
	cphase.record(actualcycle,actualphasetime[0], actualphasetime[3], ntime);
	cyclelist.push_back(cphase);

	return cyclelist.size();
}

void CNode::reportCycle()
{
	int node = GetID();
	ofstream outfile;
	if ( node == 28)		outfile.open("C:\\TSIS6 Projects\\BPS_project\\final_input\\node28.xls"); //output trf content into text file (for test)
	else if ( node == 2)outfile.open("C:\\TSIS6 Projects\\BPS_project\\final_input\\node2.xls"); //output trf content into text file (for test)
	else if ( node == 3)outfile.open("C:\\TSIS6 Projects\\BPS_project\\final_input\\node3.xls"); //output trf content into text file (for test)
	else if ( node == 4)outfile.open("C:\\TSIS6 Projects\\BPS_project\\final_input\\node4.xls"); //output trf content into text file (for test)
	else if ( node == 5)outfile.open("C:\\TSIS6 Projects\\BPS_project\\final_input\\node5.xls"); //output trf content into text file (for test)
	else if ( node == 6)outfile.open("C:\\TSIS6 Projects\\BPS_project\\final_input\\node6.xls"); //output trf content into text file (for test)
	else if ( node == 7)outfile.open("C:\\TSIS6 Projects\\BPS_project\\final_input\\node7.xls"); //output trf content into text file (for test)
	else if ( node == 8)outfile.open("C:\\TSIS6 Projects\\BPS_project\\final_input\\node8.xls"); //output trf content into text file (for test)
	else if ( node == 9)outfile.open("C:\\TSIS6 Projects\\BPS_project\\final_input\\node9.xls"); //output trf content into text file (for test)
	outfile<< "cycle \t phase1 \t phase2 \t time\n";
	for (cycle_pos=cyclelist.begin(); cycle_pos != cyclelist.end(); cycle_pos++)
	{
		int ctime = (*cycle_pos).getCycle()+ 1;
		int cphase1 = (*cycle_pos).getPhaseTime1();
		int cphase2 = (*cycle_pos).getPhaseTime2();
		int ntime = (*cycle_pos).getTime();
		outfile<< ctime << "\t"<< cphase1<< "\t"<< cphase2<<"\t"<<ntime<<"\n";		
	}

}
