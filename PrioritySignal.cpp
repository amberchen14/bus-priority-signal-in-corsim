#include "PrioritySignal.h"


CPriority::CPriority() 
{
	cycle1 = 0;
	cycle2 = 0;
	phase_number = 0;
	addgrn = 0;
	dltred = 0;
	mingrn = 12;
	realcycle = 0;
	RealPhase = 0;
	for (int i = 0; i < 12; i ++)
	{
		 phasetime[i] = 0;
		 phasetime2[i] = 0;
	     realphase[i] = 0;
		for (int j = 0; j < 5; j ++)
		{
			phasesignal[i][j] = 0;
		}
	}
}

CPriority::~CPriority()
{
}


void CPriority::ID(int id)
{
	NodeID = id;
}

int CPriority::returnID()
{
	return NodeID;
}


void CPriority::downnode(int node)
{
	dwnnodelist.push_back(node);
}

int CPriority::returndownnode(int i)
{
	return dwnnodelist[i];
}

int CPriority::recordTimeGap(int i)
{
	if (i > 0) timegap = 0;
	else timegap++;

	return 1;
}

int CPriority::returnTimeGap()
{
	return timegap;
}

int CPriority::recordCrossVehicles(int i)
{
	for (int veh = 0; veh < vehlist.size(); veh++)
	{
		if (vehlist[veh]==i) return 0;
	}
	vehlist.push_back(i);
	return 1;

}


void CPriority::phase_signal(int phase, int order, int signal)
{
	phasesignal[phase][order] = signal;
	phase_number = phase;
}


void CPriority::setPhaseTime(int phase, int time)
{
	phasetime[phase] = time; //default
	phasetime2[phase] = time; // realtime
	cycle1 += time;
	cycle2 = cycle1;
	interval = 0;
}


