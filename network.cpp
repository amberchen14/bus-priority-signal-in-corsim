FILE * pFile;
long lSize;
char * buffer;
size_t result;

int number_of_lines = 0;
int number_of_chars = 0;

void CNetwork:: CreateTRF()
{
	ifstream fin;
	//fin.open("C:\\TSIS6 Projects\\DTA_BPS\\Guadalupe.trf", ios::in);

	fin.open("C:\\TSIS6 Projects\\BPS_project\\final_input\\impact_scenario\\video\\impact_scenario.trf", ios::in);
   sprintf_s( gsOutput, "open file " );
   OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
	char my_character ;
	while (!fin.eof() ) 
	{
		fin.get(my_character);
		number_of_chars++;
		pTRF.push_back(my_character);
		cout << my_character;  //print characters
		if (my_character == '\n')
		{
			++number_of_lines;
		}
	}
	//cout << "NUMBER OF LINES: " << number_of_lines << endl;
	fin.close();

	cardtrf newcard;
	newcard.readTRF(pTRF);

	 sprintf_s( gsOutput, "read file" );
	 OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
	for (int i = 0; i < number_of_lines + 1 ;i++) //get Card Type
	{
		int cardtype = 0;
		if (pTRF[i * 81 + 0]== '<') break;
		if (pTRF[i * 81 + 77] -'0' >= 0) cardtype +=  (pTRF[i *81 + 77 ] -'0')* 100;
		if (pTRF[i * 81 + 78] -'0' >= 0) cardtype +=  (pTRF[i*81+78] -'0')* 10;
		if (pTRF[i * 81 + 79] -'0' >= 0) cardtype +=  (pTRF[i*81+79] -'0')* 1;
		if (cardtype == 0 && pTRF[i * 81 + 0] -'0' == 0 ) break;
		sprintf_s( gsOutput, "line %d cardtype %d", i, cardtype );
		OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
		//cout << i <<"\t" <<  cardtype << "\n";
		
		cardtrf ss;
		ss.CardType(i, cardtype);
		cardlist.push_back(ss);
		int size = cardlist.size();
		//cout<<  size <<"\n";
	}
//free (buffer);
}

void CNetwork:: RecordType()
{

	for (card_pos = cardlist.begin(); card_pos!= cardlist.end(); card_pos ++)
	{
		int line = (*card_pos).returnLine() ;
		int cardtype = (*card_pos).returnType();	
		if ((*card_pos).returnType() == 36) Type36(line);
		//if ((*card_pos).returnType() == 187) Type187(line);
	}
	for (card_pos = cardlist.begin(); card_pos!= cardlist.end(); card_pos ++)
	{
		int line = (*card_pos).returnLine() ;
		int cardtype = (*card_pos).returnType();	
		if ((*card_pos).returnType() == 35) Type35(line);
	}

	/*
	for(bps_pos=bpslist.begin(); bps_pos!= bpslist.end(); bps_pos++)
	{
		int node = (*bps_pos).returnID();
        sprintf_s( gsOutput, "signal node %d", node );
         OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
		for (int i = 0; i < 12; i ++)
		{
			for (int j = 0; j < 4; j ++)
			{
				node = (*bps_pos).returndownnode(j);
				int control= (*bps_pos).returnSignal(i, j);
				if (node==0 || control ==0) break;

				sprintf_s( gsOutput, "phase %d node %d signal %d", i, node, control );
				OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
			}
		}
	}
	sprintf_s( gsOutput, "bus route");
	OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );	
	for (bs_pos=busroute.begin(); bs_pos!=busroute.end();bs_pos++)
	{
		sprintf_s( gsOutput, "%d", (*bs_pos));
		OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );		
	}
	
	for (bps_pos=bpslist.begin(); bps_pos!=bpslist.end();bps_pos++)
	{
		int node = (*bps_pos).returnID();
		int phase_number = (*bps_pos).returnPhaseNum();
		(*bps_pos).PriorityPhase(0);
		(*bps_pos).UnPriorityPhase(1);
		sprintf_s( gsOutput, "node %d total phase number %d", node,  phase_number);
		OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );	
	}
	*/
}

int CNetwork::Type35(int line)
{
	int node1 = 0;
	int offset = 0;

	if (pTRF[line*81] -'0' >= 0)  node1 +=  (pTRF[line*81] -'0')* 1000;
	if (pTRF[line*81 + 1] -'0' >= 0)  node1 +=  (pTRF[line*81 + 1] -'0')* 100;
	if (pTRF[line*81 + 2] -'0' >= 0)  node1 +=  (pTRF[line*81 + 2] -'0')* 10;
	if (pTRF[line*81 + 3] -'0' >= 0)  node1 +=  (pTRF[line*81 + 3] -'0')* 1;	

	int test = -1;
	for(bps_pos=bpslist.begin(); bps_pos!= bpslist.end(); bps_pos++)
	{
		if ((*bps_pos).returnID()==node1) 
		{
			test=node1;
			break;
		}
	}
	if (test == -1) return 0;

	if (pTRF[line*81 + 4] -'0' >= 0)  offset +=  (pTRF[line*81 + 4] -'0')* 1000;
	if (pTRF[line*81 + 5] -'0' >= 0)  offset +=  (pTRF[line*81 + 5] -'0')* 100;
	if (pTRF[line*81 + 6] -'0' >= 0)  offset +=  (pTRF[line*81 + 6] -'0')* 10;
	if (pTRF[line*81 + 7] -'0' >= 0)  offset +=  (pTRF[line*81 + 7] -'0')* 1;	

	for (int i = 0; i < 5; i ++)
	{
		int node2 = 0;
		if (pTRF[line*81 + 8  + 4*i] -'0' >= 0)  node2 +=  (pTRF[line*81 + 8  + 4*i] -'0')* 1000;
		if (pTRF[line*81 + 9  + 4*i] -'0' >= 0)  node2 +=  (pTRF[line*81 + 9  + 4*i] -'0')* 100;
		if (pTRF[line*81 +10  + 4*i] -'0' >= 0)  node2 +=  (pTRF[line*81 + 10 + 4*i] -'0')* 10;
		if (pTRF[line*81 +11  + 4*i] -'0' >= 0)  node2 +=  (pTRF[line*81 + 11 + 4*i] -'0')* 1;		

		if (node2==0) break;
		else
		{
			int test = 0;
			for(bps_pos=bpslist.begin(); bps_pos!= bpslist.end(); bps_pos++)
			{
				if ((*bps_pos).returnID()==node1)
				{
					(*bps_pos).downnode(node2);
				}		
			}			
		
		}
	}
	for (int i = 0; i < 12; i ++)
	{
		int time = 0;
		if (pTRF[line*81 +29  + 4*i] -'0' >= 0)  time +=  (pTRF[line*81 + 29 + 4*i] -'0')* 100;
		if (pTRF[line*81 +30  + 4*i] -'0' >= 0)  time +=  (pTRF[line*81 + 30 + 4*i] -'0')* 10;
		if (pTRF[line*81 +31  + 4*i] -'0' >= 0)  time +=  (pTRF[line*81 + 31 + 4*i] -'0')* 1;
		if (time == 0) break;
		sprintf_s( gsOutput, "node %d phase %d phasetime %d", node1, i, time );
		OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );	
		for(bps_pos=bpslist.begin(); bps_pos!= bpslist.end(); bps_pos++)
		{
			if ((*bps_pos).returnID()==node1)
			{
				(*bps_pos).setPhaseTime(i, time);
			}		
		}	
	}

	return 1;
}

int CNetwork::Type36(int line)
{
	int node1 = 0;
	int control = 0;
	int ext = 0;

	if (pTRF[line*81] -'0' >= 0)  node1 +=  (pTRF[line*81] -'0')* 1000;
	if (pTRF[line*81 + 1] -'0' >= 0)  node1 +=  (pTRF[line*81 + 1] -'0')* 100;
	if (pTRF[line*81 + 2] -'0' >= 0)  node1 +=  (pTRF[line*81 + 2] -'0')* 10;
	if (pTRF[line*81 + 3] -'0' >= 0)  node1 +=  (pTRF[line*81 + 3] -'0')* 1;	

	if (pTRF[line*81 + 76] -'0' >= 0)  ext = pTRF[line*81 + 76] -'0';
	
	if (ext == 0) return 0;

	CPriority ss;
	ss.ID(node1);
	bpslist.push_back(ss);

	sprintf_s( gsOutput, "signal node %d size %d", node1, bpslist.size() );
	OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
	for(bps_pos=bpslist.begin(); bps_pos!= bpslist.end(); bps_pos++)
	{
		if ((*bps_pos).returnID()==node1) 
		{
			for (int i = 0; i < 12; i ++)
			{
				for (int j = 0; j < 5; j ++)
				{
					if (pTRF[line*81 + 5 + j + 5*i] -'0' > 0)  
					{
						control = (pTRF[line*81 + 5 + j + 5*i] -'0');
						(*bps_pos).phase_signal(i,j,control);
						sprintf_s( gsOutput, "phase %d order %d control %d",  i, j, control );
						OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
					}
					else if (j == 0 ) break;
					else continue;
				}
			}
			break;	
		}
	}
	return 1;
}


void CNetwork::InitialSet()
{
   CNode* pNode = NULL;
   POSITION pos = m_NodeList.GetHeadPosition();
   sprintf_s( gsOutput, "for test: InitialSet()");
   OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );	
   while( pos != NULL )
   {
      pNode = m_NodeList.GetNext( pos );
	  pNode->setPhaseTime();
      if( pNode->GetControlType() == CString("external") )
      {
		  int node = pNode->GetID();
		  for (int i = 0; i <12; i ++)
		  {
			int phase = pNode->getPhaseTime(i) ;
			if (phase == 0) break;
			sprintf_s( gsOutput, "node %d, phase %d, time %d",node, i, phase);
			OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );				
		  }
		  int cycletime = pNode->getCycleTime();
		  sprintf_s( gsOutput, "node %d, cycle time %d",node, cycletime );
		  OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );		
	  }
   }
}


int CNetwork::TraceBusLocation()
{
   //loop through vehicles to find all EVs
	//sprintf_s( gsOutput, " TraceBusLocation() ");
	//OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
	int nTime = sclock ;
	int shtdst =-1;
	int Dis2Sig = -1;
	for (int i = 0; i < 20; i ++)
	{
		bus_num[i][0] = 0;  //0 = id, 1=length, 2=traveltime, 3=upnode, 4=dwnode, 5=new_cross_street_vehnum, 6=trigger
		bus_num[i][1] = 10000;
		bus_num[i][2] = 10000;
		bus_num[i][3] = 0;
		bus_num[i][4] = 0;
		bus_num[i][5] = 0;
		bus_num[i][6] = 0;
	}
	int bus_number = 0;
	int vehID = 0;
	int bus_unode = 0;
	int bus_dnode = 0;
	for (int iv = 0; iv < ttlveh; iv++ )
	{
	   if( nfleet[iv] == 3)
	   {
            vehID = netgvh[iv];
		   int LinkID = nvhlnk[iv] - 1;
		   if (LinkID < 0 ) continue;
		   int ttime = sclock - entime[iv] / 10 + 1;
           int bus_speed = spdln[iv];
		   bus_dnode = NETSIM_LINKS_mp_DWNOD[LinkID] ;
		   bus_unode = NETSIM_LINKS_mp_UPNOD[LinkID];
		   if( bus_unode < 7000 ) bus_unode = nmap[bus_unode-1];
		   if( bus_dnode < 7000 ) bus_dnode = nmap[bus_dnode-1];

		   int busid =  nbusiv[iv];
		   int route = busrt[busid];
		   Dis2Sig = xlngth1[LinkID] - distup[iv]; //bus distance to signal

			if (bus_speed > 0) shtdst= Dis2Sig/bus_speed;
			else shtdst = 10000;
			bus_num[bus_number][0] = vehID;
			bus_num[bus_number][1] = Dis2Sig;
			bus_num[bus_number][2] = shtdst;
			bus_num[bus_number][3] = bus_unode;
			bus_num[bus_number][4] = bus_dnode;
		//	sprintf_s( gsOutput, "bus id %d length %d speed %d travel time %d upnode %d dnode %d ",bus_num[bus_number][0], bus_num[bus_number][1], bus_speed, bus_num[bus_number][2], bus_num[bus_number][3], bus_num[bus_number][4]);
		//	OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );	
			bus_number ++;
			continue;
		   
	   }
   }
	for (int iv = 0; iv < ttlveh; iv++ )
	{
		if( nfleet[iv] != 3)
		{
			vehID = netgvh[iv];
			int LinkID = nvhlnk[iv] - 1;
			if (LinkID < 0 ) continue;
			int dnode = NETSIM_LINKS_mp_DWNOD[LinkID] ;
			int unode = NETSIM_LINKS_mp_UPNOD[LinkID];
			if( unode < 7000 ) unode = nmap[unode-1];
			if( dnode < 7000 ) dnode = nmap[dnode-1];
			for (int i = 0; i < 20; i ++)
			{
				if (bus_num[i][0] == 0) break;
				if (bus_num[i][3] != unode && bus_num[i][4] == dnode)
				{	
					for (bps_pos = bpslist.begin(); bps_pos != bpslist.end(); bps_pos++)
					{
						if (bus_num[i][4] == (*bps_pos).returnID())
						{
							bus_num[i][5]+=(*bps_pos).recordCrossVehicles(vehID);
							break;
						}
					}
					break;
				}
			}

		}
	}
	for (int i = 0; i < 20; i ++)
	{
		if (bus_num[i][0] <= 0) break;
		for (bps_pos = bpslist.begin(); bps_pos != bpslist.end(); bps_pos++)
		{
			if (bus_num[i][4] == (*bps_pos).returnID())
			{
				(*bps_pos).recordTimeGap(bus_num[i][5]);
				bus_num[i][6] = (*bps_pos).returnTimeGap();
			//	sprintf_s( gsOutput, " dnode %d cross_street %d trigger %d ",  bus_num[i][4], bus_num[i][5], bus_num[i][6]);
			//	OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
			}
		}
	}
	return -1;
} 

int CNetwork::Trigger(int dnode, int flag)
{
	if (flag == 1)
	{
		for (int i = 0; i < 20; i ++)
		{
			if (bus_num[i][0] == 0) break;
			if (bus_num[i][4] == dnode) 
			{
			//	sprintf_s( gsOutput, " bus id %d length %d travel time %d upnode %d dnode %d cross_street %d trigger %d ", bus_num[i][0], bus_num[i][1], bus_num[i][2], bus_num[i][3], bus_num[i][4], bus_num[i][5], bus_num[i][6]);
			//	OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );					
				if ( bus_num[i][1] <= 180 && bus_num[i][2] < 8) return bus_num[i][2];	
				else return -1;
			}
		}		
	}

	if (flag == 3) 
	{
		for (int i = 0; i < 20; i ++)
		{
			if (bus_num[i][0] == 0) break;
			if (bus_num[i][4] == dnode && bus_num[i][1] <= 180) 
			{
					return 1;
			}
		}
	}

	return -1;
} 

void CNetwork::UpdatePriorityNodeSignalStates()
{
   // Update the signal states for all the
   // nodes not controlled by CORSIM.
   int nTime = sclock + giEndOfInit;

   CNode* pNode = NULL;
   POSITION pos = m_NodeList.GetHeadPosition();
   TraceBusLocation();
   while( pos != NULL )
   {
      pNode = m_NodeList.GetNext( pos );
      if( pNode->GetControlType() == CString("external") )
      {
		  int node = pNode->GetID();
		  if (nTime < 12 && node== 9 ) continue;
		  if (nTime < 23 && node ==8) continue;
		  if (nTime < 34 && node== 7)continue;
		  if (nTime < 45 && node ==6) continue;
		  if (nTime < 55 && node ==5) continue;
			if (nTime < 65 && node== 4)continue;
			if (nTime < 6 && node ==3) continue;
			if (nTime< 16 && node ==2)continue;

		  //if (node %2 == 0 && nTime < 20)continue;
		  int flag = pNode->UpdateSignal(nTime); 
		  int phase = pNode->getPhase();
		  int phasetime = pNode->getPhaseTime();


			  
		  if (flag == 1 || flag == 3)
		  {
			  int response = Trigger(node, flag);
			  if (flag == 3)
			  {
		//		sprintf_s( gsOutput, "BUS response %d node %d time %d", response, node, nTime);
		//		OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );	
			  }
			  if (flag == 1) flag = pNode->GrnExtension(response);
			  if (flag == 3) pNode->RedTruncation(response);

		  }
		  //int cycletime = pNode->getCycleTime();
		//  int phase = pNode->getPhase();
		//  int phasetime = pNode->getPhaseTime();

		  if (node ==4)
		  {
		//	sprintf_s( gsOutput, "node %d, cycle time %d phase %d phastime %d flag %d time %d ",node, cycletime, phase, phasetime, flag, nTime );
		//	OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );	
		  }
         // Node is not controlled by CORSIM.
		  if (flag==2 || (nTime == 1 && node== 28)|| (nTime == 12 && node ==9) || (nTime == 23 && node ==8) || (nTime == 34 && node== 7)|| (nTime == 45 && node ==6) || (nTime == 55 && node ==5) || (nTime == 65 && node== 4)|| (nTime == 6 && node ==3) || (nTime == 16 && node ==2) ) 
		  {
		//	sprintf_s( gsOutput, "node %d, time %d activate ",node, nTime );
		//	OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );	
			pNode->SetSignalState();
		  }
		  //  if (flag==2 || (nTime == 20 && node % 2 == 0)|| (nTime == 1 && node % 2 ==1 )) pNode->SetSignalState();

	  }
   }
}
void CNetwork::SignalCode( int nSignalCode)
{
	int S_RED    =  0;
	int	S_AMBER  =  1;
    int S_GREEN  =  2;
    int S_PERGRN =  3;  // permitted green
    int S_NONE   =  4;
    int S_PERAMB =  5;  // amber following a permitted green
    int S_STOP   = 30;
    int S_YIELD  = 31;

	switch( nSignalCode )
    {
		case 0:
  			 getLeft = S_AMBER;
			 getThrough = S_AMBER;
			 getRight = S_AMBER;
			 getLeftDiag = S_AMBER;    
			 break;

		 case 1:  // green ball
			 getLeft = S_GREEN;
			 getThrough = S_GREEN;
			 getRight = S_GREEN;
			 getLeftDiag = S_GREEN;
            
			 break;

         case 2:  // red ball
            getLeft = S_RED;
            getThrough = S_RED;
            getRight = S_RED;
            getLeftDiag = S_RED;

            break;

         case 3:  // green right turn only
            getLeft = S_RED;
            getThrough = S_RED;
            getRight = S_GREEN;
            getLeftDiag = S_RED;
  
            break;

         case 4:  // green left turn only
            getLeft = S_GREEN;
            getThrough = S_RED;
            getRight = S_RED;
            getLeftDiag = S_RED;

            break;

         case 5:  // stop sign
            getLeft = S_RED;
            getThrough = S_RED;
            getRight = S_RED;
            getLeftDiag = S_RED;

            break;

         case 6:  // green diagonal only
            getLeft = S_RED;
            getThrough = S_RED;
            getRight = S_RED;
            getLeftDiag = S_GREEN;

            break;

         case 7:  // green through only
            getLeft = S_RED;
            getThrough = S_GREEN;
            getRight = S_RED;
            getLeftDiag = S_RED;

            break;

         case 8:  // green right and left turn only
            getLeft = S_GREEN;
            getThrough = S_RED;
            getRight = S_GREEN;
            getLeftDiag = S_RED;

            break;

         case 9:  // green thru and right only
            getLeft = S_RED;
            getThrough = S_GREEN;
            getRight = S_GREEN;
            getLeftDiag = S_RED;
            break;

         default:  // invalid code
            getLeft = S_NONE;
            getThrough = S_NONE;
            getRight = S_NONE;
            getLeftDiag = S_NONE;
            break;
      }
}
int CNetwork::TravelTime()
{
   int nTime = sclock + giEndOfInit;
   if (giEndOfInit ==0) return 0;

  // sprintf_s( gsOutput, "Initial Time %d nTime %d real time %d total car %d total vehicles %d",giEndOfInit, nTime, sclock, automobile.size(), ttlveh);
  // OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );

   for (int iv = 0; iv < ttlveh; iv++ )
   {
	int vehID = netgvh[iv];
	int LinkID = nvhlnk[iv] - 1;
	if (LinkID < 0)
	{
	//	sprintf_s( gsOutput, "wield vehicle id %d time %d", vehID, sclock);
	//    OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
		continue;
	}
	int dnode = NETSIM_LINKS_mp_DWNOD[ LinkID];
	int unode = NETSIM_LINKS_mp_UPNOD[ LinkID];
	if( dnode < 7000 ) dnode = nmap[dnode-1];
	if( unode < 7000 ) unode = nmap[unode-1];

	//sprintf_s( gsOutput, "vehicle id %d, unode %d, dnode %d", vehID, unode, dnode);
	//OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
	
	if (nfleet[iv]==3)
	{
		int flag = 0;
		for (bus_pos=bus.begin(); bus_pos!=bus.end(); bus_pos++)
		{
			if((*bus_pos).returnID()==vehID) 
			{
				flag = 1;
		//		sprintf_s( gsOutput, "flag %d,bus %d", flag, vehID);
		//		OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
				int starttime = (*bus_pos).returnStartTime();
				int start_unode =(*bus_pos).returnStartUp();
				int start_dnode =(*bus_pos).returnStartDn();

				(*bus_pos).VehiclePosition(starttime ,start_unode, start_dnode, nTime, unode, dnode)	;
		//		sprintf_s( gsOutput, "end bus id %d, endtime %d  endpos %d", vehID, nTime, LinkID);
		//		OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
				break;
			}
		}
		if (flag == 0)
		{
			CVehicle pVehicle;	
			pVehicle.VehicleID(vehID);
			pVehicle.VehiclePosition(nTime, unode, dnode, nTime, unode, dnode)	;	
			bus.push_front(pVehicle);
		//	sprintf_s( gsOutput, "start bus id %d, starttime %d  startpos %d", vehID,  nTime, LinkID);
		//	OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
		}
	}
	else 
	{
		int flag = 0;
		for (auto_pos=automobile.begin(); auto_pos<automobile.end(); auto_pos++)
		{
			if((*auto_pos).returnID()==vehID) 
			{
				flag = 1;
		//		sprintf_s( gsOutput, "flag %d car %d",flag,  vehID);
		//		OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
				int starttime = (*auto_pos).returnStartTime();
				int start_unode = (*auto_pos).returnStartUp();
				int start_dnode =(*auto_pos).returnStartDn();
				(*auto_pos).VehiclePosition( starttime,start_unode, start_dnode, nTime, unode, dnode)	;
		//		sprintf_s( gsOutput, "car %d starttime %d, up %d, dn %d",vehID, starttime, start_unode, start_dnode);
		//		OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
				break;
			}

		}
		if (flag == 0)
		{		
	//		sprintf_s( gsOutput, "flag %d car %d link %d",flag,  vehID, LinkID);
	//		OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
			CVehicle pVehicle;	
			pVehicle.VehicleID(vehID);
			pVehicle.VehiclePosition(nTime, unode, dnode, nTime, unode, dnode)	;	
			automobile.push_front(pVehicle);
			
		}
	}
   }
 //  sprintf_s( gsOutput, "\n");
 //  OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
   return 1;
}
void CNetwork::SetRequireData()
{
   // Find the CORSIM link ID for the link (upnode, dnnode).
   int index = 0;
   int dnode = 0;
   int unode = 0;
   // Search through all the links in CORSIM.
   for( index=0; index<ttlnk; index++ )
   {
      // For the ith link, get the downstream node and
      // upstream node as represented in CORSIM.
      dnode = NETSIM_LINKS_mp_DWNOD[index];
      unode = NETSIM_LINKS_mp_UPNOD[index];

      // For non-source nodes (<7000), use the nmap array to map
      // the no number in CORSIM back to the user defined node
      // number in the TRAF file.  Use an offset of -1 (i.e., dnode-1),
      // because C arrays start at 0; FORTRAN arrays start at 1.
      if( dnode < 7000 ) dnode = nmap[dnode-1];
      if( unode < 7000 ) unode = nmap[unode-1];

	sprintf_s( gsOutput, "Link ID %d upnode %d downnode %d \n",index, unode, dnode );
	OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
	 if(unode==32 && dnode==28) // if(unode==14 && dnode==6)
	 {
		originlink=index;
	 }
	 if(unode==2 && dnode== 1)
	 {
		destlink=index;
	 }
	
   }

//	sprintf_s( gsOutput, "startlink %d destlink %d \n",originlink, destlink);
//	OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
}
void CNetwork::ReportTravelTime()
{//
	ofstream cartime ("C:\\TSIS6 Projects\\BPS_project\\final_input\\autotime.xls"); //output trf content into text file (for test)
	//ofstream cartime ("C:\\TSIS6 Projects\\BPS_project\\BPS_fixed\\BPS\\autotime.xls"); //output trf content into text file (for test)
	cartime <<"vehicle ID \t start time \t upnode\t dnode \t end time \t upnode\t dnode \n "	;	
   // Search through all the links in CORSIM.


	for (auto_pos=automobile.begin(); auto_pos<automobile.end(); auto_pos++)
	{
		int vehid=  (*auto_pos).returnID();
		int starttime = (*auto_pos).returnStartTime();
		int start_up = (*auto_pos).returnStartUp();
		int start_dn =  (*auto_pos).returnStartDn();
		int endtime = (*auto_pos).returnEndTime();
		int end_up = (*auto_pos).returnEndUp();
		int end_dn =  (*auto_pos).returnEndDn();
		//int endpos = (*auto_pos).returnEndPos();

		if (starttime < 1860) continue;
		//sprintf_s( gsOutput, "id %d start pos %d end pos %d\n",vehid,  startpos,endpos) ;
		//OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
		cartime << vehid<<"\t"<<starttime<<"\t" <<start_up<<"\t"<< start_dn<<"\t"<<endtime<<"\t"<<end_up<<"\t"<<end_dn<<"\n";
	}
	ofstream par ("C:\\TSIS6 Projects\\BPS_project\\final_input\\partime.xls"); //output trf content into text file (for test)
	//ofstream cartime ("C:\\TSIS6 Projects\\BPS_project\\BPS_fixed\\BPS\\autotime.xls"); //output trf content into text file (for test)
	par <<"vehicle ID \t start time \t upnode\t dnode \t end time \t upnode\t dnode \n "	;	
   // Search through all the links in CORSIM.

	int num = 0;
	int tt = 0;
	float avg = 0;
	for (auto_pos=automobile.begin(); auto_pos<automobile.end(); auto_pos++)
	{
		int vehid=  (*auto_pos).returnID();
		int starttime = (*auto_pos).returnStartTime();
		int start_up = (*auto_pos).returnStartUp();
		int start_dn =  (*auto_pos).returnStartDn();
		int endtime = (*auto_pos).returnEndTime();
		int end_up = (*auto_pos).returnEndUp();
		int end_dn =  (*auto_pos).returnEndDn();
		//int endpos = (*auto_pos).returnEndPos();

		if (starttime < 1860) continue;
		if (start_up != 32 || start_dn != 28 || end_up!= 2 || end_dn!= 1 ) continue;
		num++;
		tt+= (endtime - starttime);
		//sprintf_s( gsOutput, "id %d start pos %d end pos %d\n",vehid,  startpos,endpos) ;
		//OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
		par << vehid<<"\t"<<starttime<<"\t" <<start_up<<"\t"<< start_dn<<"\t"<<endtime<<"\t"<<end_up<<"\t"<<end_dn<<"\n";
	}
	if (num != 0) avg = float (tt/num);
	par <<  num<<"\t"<<tt<<"\t"<< avg<<"\n";

	//ofstream bustime ("C:\\TSIS6 Projects\\BPS_project\\BPS_fixed\\BPS\\bustime.xls"); //output trf content into text file (for test)
	ofstream bustime ("C:\\TSIS6 Projects\\BPS_project\\final_input\\bustime.xls"); //output trf content into text file (for test)
	bustime<<"Bus ID \t start time \t upnode\t dnode \t end time \t upnode\t dnode \n "	;
	
	tt = 0;
	avg = 0;
	num = 0;
	for (bus_pos=bus.begin(); bus_pos < bus.end(); bus_pos++)
	{
		int vehid=  (*bus_pos).returnID();
		int starttime = (*bus_pos).returnStartTime();
		int start_up = (*bus_pos).returnStartUp();
		int start_dn =  (*bus_pos).returnStartDn();
		int endtime = (*bus_pos).returnEndTime();
		int end_up = (*bus_pos).returnEndUp();
		int end_dn =  (*bus_pos).returnEndDn();
		//int endpos = (*auto_pos).returnEndPos();
		//sprintf_s( gsOutput, "id %d start pos %d end pos %d\n",vehid,  startpos,endpos) ;
		//OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );
		if (starttime < 1860) continue;
		if (start_up != 32 || start_dn != 28 || end_up!= 2 || end_dn!= 1 ) continue;
		num++;
		tt+= (endtime - starttime);
		bustime << vehid<<"\t"<<starttime<<"\t" <<start_up<<"\t"<< start_dn<<"\t"<<endtime<<"\t"<<end_up<<"\t"<<end_dn<<"\n";
	}

	if (num != 0) avg = float (tt/num);
	bustime << num<<"\t"<<tt<<"\t"<< avg<<"\n";
}
void CNetwork::ReportVolume()
{
	int index = 0;
	int dnode = 0;
	int unode = 0;
   // Search through all the links in CORSIM.
	//ofstream link_vlm ("C:\\TSIS6 Projects\\BPS_project\\final_input\\impact_scenario\\traditional_no_compensation\\volume.xls"); //output trf content into text file (for test)
	//link_vlm <<"link ID \t upnode \t dnnode\t left \t thru \t right \n ";			
	for( index=0; index<ttlnk; index++ )
	{
		// For the ith link, get the downstream node and
		// upstream node as represented in CORSIM.
		dnode = NETSIM_LINKS_mp_DWNOD[index];
		unode = NETSIM_LINKS_mp_UPNOD[index];

		// For non-source nodes (<7000), use the nmap array to map
		// the no number in CORSIM back to the user defined node
		// number in the TRAF file.  Use an offset of -1 (i.e., dnode-1),
		// because C arrays start at 0; FORTRAN arrays start at 1.
		if( dnode < 7000 ) dnode = nmap[dnode-1];
		if( unode < 7000 ) unode = nmap[unode-1];

		int left = cumvl[index];
		int right = cumvr[index];
		int thru =  cumveh[index] - left - right ;


		//sprintf_s( gsOutput, " upnode %d downnode %d thru %d left %d right %d", unode, dnode, thru, right, left);
		//OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );	 
		//link_vlm << index<<"\t"<<unode<<"\t" <<dnode<<"\t"<< left <<"\t"<<thru<<"\t"<<right<<"\n";
	}

}
void CNetwork::ReportPhase()
{

   int nTime = sclock + giEndOfInit;

   CNode* pNode = NULL;
   POSITION pos = m_NodeList.GetHeadPosition();

	while( pos != NULL )
   {
	   pNode = m_NodeList.GetNext( pos );
	   if(pNode->GetControlType() == CString("external")) 
	   {
		int node = pNode->GetID();
		sprintf_s( gsOutput, " Got it, node %d", node);
		OutputString( gsOutput, strlen(gsOutput), SIM_COLOR_RGB, RTE_MESSAGE_RGB );	 
		pNode->reportCycle();
		continue;
	  }
   }

}